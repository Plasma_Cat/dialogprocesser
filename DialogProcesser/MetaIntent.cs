﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace DialogProcesser
{
    class MetaIntent
    {
        public SpeakerPerson FirstPerson;
        public SpeakerPerson SecondPerson;
        
        public string IntentDisplayedName = string.Empty;
        public string Locale = "en-US";

        /// <summary>
        /// JSON serialized message object
        /// </summary>
        [JsonIgnore]
        public string JsonMetaIntent
        {
            get { return Newtonsoft.Json.JsonConvert.SerializeObject(this); }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using NAudio.Wave;
using NAudio.Lame;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using static Google.Cloud.Speech.V1.RecognitionConfig.Types;

namespace DialogProcesser
{
    /// <summary>
    /// Singleton class for audio dialog processing
    /// </summary>
    public class DialogWorkerSingleton
    {
        #region Singleton staff
        private static DialogWorkerSingleton dws = null;
        private static readonly object syncObject = new object();

        private static string url = "https://speech.googleapis.com/v1p1beta1/speech:recognize?key=AIzaSyAjhcGS26gCw3RUs_d2OcA6N8hh2g6flZk";

        private DialogWorkerSingleton() { }

        public static DialogWorkerSingleton Instance
        {
            get
            {
                lock (syncObject)
                {
                    if (dws == null)
                    {
                        dws = new DialogWorkerSingleton();
                    }

                    return dws;
                }
            }
        }
        #endregion Singleton staff

        /// <summary>
        /// Get dialog in json format from target directory with *.mp3 files
        /// </summary>
        /// <param name="directoryPath">Path to directory contains *.mp3</param>
        /// <returns>json dialog</returns>
        public async Task<string> GetDialogJson(string directoryPath)
        {
            var metaIntents = new List<string>();

            try
            {
                // open files and create metaintents from that
                var files = Directory.GetFiles(directoryPath, "*.mp3", SearchOption.AllDirectories).ToList();

                int i = 0;
                var persons = new List<SpeakerPerson>();
                //get streams
                foreach (var wav in files)
                {
                    // get timestamps from file names
                    var wavPathWithoutExt = Path.GetFileNameWithoutExtension(wav);
                    //1_00-14-07_00-18-05
                    var parts = wavPathWithoutExt.Split("_");
                    var msms_st = parts[1].Split("-").Select(x => int.Parse(x)).ToArray<int>();
                    var msms_ed = parts[2].Split("-").Select(x => int.Parse(x)).ToArray<int>();

                    var st = new TimeSpan(0, 0, msms_st[0], msms_st[1], msms_st[2]);
                    var ed = new TimeSpan(0, 0, msms_ed[0], msms_ed[1], msms_ed[2]);

                    var person1 = new SpeakerPerson();

                    var bytes = File.ReadAllBytes(wav);

                    person1 = await Instance.Recognize(new MemoryStream(bytes), st, ed, (i & 1) == 0 ? SpeakerType.@operator : SpeakerType.user, "nb-NO");
                    persons.Add(person1);

                    i++;
                }

                // get metaintents
                for (int j = 0; j < persons.Count - 1; j += 2)
                {
                    var metaIntent = Instance.DialogToJson(persons[j], persons[j + 1], "nb-NO");
                    metaIntents.Add(metaIntent.MessageData.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

             return JsonConvert.SerializeObject(metaIntents, Formatting.Indented);
        }

        #region Staff
        /// <summary>
        /// Create a Speaker one from the speech sample
        /// </summary>
        /// <param name="SpeechSample">Audio stream of speech sample</param>
        /// <param name="StartRep">Time stamp of start this samle</param>
        /// <param name="EndRep">Time stamp of end of this sample</param>
        /// <param name="spType">Speaker's role</param>
        /// <param name="locale">Language code</param>
        /// <returns>Filled person object</returns>
        private async Task<SpeakerPerson> Recognize(Stream SpeechSample, TimeSpan StartRep, TimeSpan EndRep, SpeakerType spType, string locale = "en-US")
        {
            var whoSaid = new SpeakerPerson()
            {
                speakerType = spType,
                Start = StartRep,
                End = EndRep,
                Text = "Not recognized."
            };

            whoSaid.Text = await SpeechToText(SpeechSample, "nb-NO", "default", 44100, 1, 2);

            return whoSaid;
        }

        /// <summary>
        /// Create jason with pair of replics
        /// </summary>
        /// <param name="firstPersonSays">First person's replica</param>
        /// <param name="secondPersonSays">Second person's replica</param>
        /// <param name="locale">Language code</param>
        /// <returns>Standart result container woth metaintent data</returns>
        private MethodMessage DialogToJson(SpeakerPerson firstPersonSays, SpeakerPerson secondPersonSays, string locale = "en-US")
        {
            var result = new MethodMessage();

            try
            {
                //TODO:
                //Get pair speech samples
                //Convert it into text
                //Get json of intent
                var metaIntent = new MetaIntent()
                {
                    FirstPerson = firstPersonSays,
                    SecondPerson = secondPersonSays,
                    Locale = locale,
                    IntentDisplayedName = Guid.NewGuid().ToString()
                };

                result.MessageData = metaIntent.JsonMetaIntent;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.MessageData = $"Error has occured into {nameof(DialogToJson)} method!";
                result.ExceptionData = ex;
            }

            return result;
        }

        /// <summary>
        /// Google Text to Speech - Dialogue parser (special config) 
        /// </summary>
        /// <returns>Json String with data </returns>
        private async Task<string> SpeechToText(Stream Body, string lang = "en-US", string model = "video", int samplerate = 48000, int channelcount = 2, int speakercount = 2)
        {
            string filePath = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + ".mp3";
            string filePath2 = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + ".wav";

            List<string> _list = new List<string>();
            var parsedPhrase = new StringBuilder();

            HttpResponseMessage response = null;
            string _responseText = "";
            try
            {
                using (var reader = new BinaryReader(Body))
                {
                    reader.BaseStream.Position = 0;
                    var jsonVoiceByteArray = reader.ReadBytes((int)Body.Length);

                    Dictionary<string, Object> _audio3, _config;
                    string jsonStr = "";

                    System.IO.File.WriteAllBytes(filePath, jsonVoiceByteArray);

                    try
                    {
                        ConvertMp3ToWav(filePath, filePath2, samplerate, 16, channelcount);
                    }
                    catch (Exception ex)
                    {
                        FileInfo currentFile = new FileInfo(filePath);
                        currentFile.Delete();
                        FileInfo currentFile2 = new FileInfo(filePath2);
                        currentFile2.Delete();
                        //Debug.WriteLine("Error on file: {0}\r\n   {1}", filePath, ex.Message);
                    }

                    byte[] audioBytes = System.IO.File.ReadAllBytes(filePath2);
                    string base64String = Convert.ToBase64String(audioBytes);

                    char[] charArray = base64String.ToCharArray();

                    string temp = "";

                    int charArrayLength = charArray.Length;

                    int iteratorCount = charArrayLength / 10000000;
                    int iteratorLastPart = charArrayLength % 10000000;

                    JObject jsonStrOutput = null;

                    if (iteratorCount > 0)
                        for (int i = 0; i < iteratorCount; i++)
                        {
                            temp = base64String.Substring(10000000 * i, 10000000 + 10000000 * i);

                            _audio3 = new Dictionary<string, Object>
                        {
                            {"content" ,  temp},
                        };

                            _config = new Dictionary<string, Object>
                        {
                            {"encoding" , AudioEncoding.Linear16 },
                            {"sampleRateHertz", samplerate},
                            {"audioChannelCount", channelcount },
                            {"languageCode" , lang},
                            {"diarizationSpeakerCount" , speakercount},
                            {"enableAutomaticPunctuation" , true},
                            {"enableSpeakerDiarization" , true},
                            {"model", model },
                            {"profanityFilter", false }
                        };

                            jsonStr = JsonConvert.SerializeObject(new { audio = _audio3, config = _config });

                            try
                            {
                                response = await SendRequestToService(jsonStr);
                            }
                            catch (Exception ex)
                            {
                                return JsonConvert.SerializeObject(new { error = ex.InnerException + " | " + ex.Message });
                            }

                            _responseText = await response.Content.ReadAsStringAsync();
                            jsonStrOutput = JObject.Parse(_responseText);
                            _list.Add(jsonStrOutput["results"].Last["alternatives"].First["words"].ToString());
                        }
                    // ToDo - redo the transition between parts
                    if (iteratorLastPart > 0)
                    {
                        temp = base64String.Substring(charArrayLength - iteratorLastPart);

                        _audio3 = new Dictionary<string, Object>
                        {
                            {"content" ,  temp},
                        };

                        _config = new Dictionary<string, Object>
                        {
                            {"encoding" , AudioEncoding.Linear16 },
                            {"sampleRateHertz", samplerate},
                            {"audioChannelCount", channelcount },
                            {"languageCode" , lang},
                            {"diarizationSpeakerCount" , speakercount},
                            {"enableAutomaticPunctuation" , true},
                            {"enableSpeakerDiarization" , true},
                            {"model", model },
                            {"profanityFilter", false }
                        };

                        jsonStr = JsonConvert.SerializeObject(new { audio = _audio3, config = _config });

                        try
                        {
                            response = await SendRequestToService(jsonStr);
                        }
                        catch (Exception ex)
                        {
                            return JsonConvert.SerializeObject(new { error = ex.InnerException + " | " + ex.Message });
                        }

                        _responseText = await response.Content.ReadAsStringAsync();
                        jsonStrOutput = JObject.Parse(_responseText);


                        _list.Add(jsonStrOutput["results"].Last["alternatives"].First["words"].ToString());

                        foreach (var word in jsonStrOutput["results"].Last["alternatives"].First["words"])
                        {
                            parsedPhrase.AppendFormat("{0} ", word["word"].ToString());
                        }

                    }


                    try
                    {
                        FileInfo currentFile = new FileInfo(filePath);
                        currentFile.Delete();
                        FileInfo currentFile2 = new FileInfo(filePath2);
                        currentFile2.Delete();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                        //Debug.WriteLine("Error on file: {0}\r\n   {1}", filePath, ex.Message);
                    }

                }

            }
            catch (Exception ex)
            {
                FileInfo currentFile = new FileInfo(filePath);
                currentFile.Delete();
                FileInfo currentFile2 = new FileInfo(filePath2);
                currentFile2.Delete();
                return JsonConvert.SerializeObject(new { error = ex.InnerException + " | " + ex.Message });
            }


            // return JsonConvert.SerializeObject(new { obj = response, list = _list });
            return parsedPhrase.ToString();
        }

        /// <summary>
        /// Special method to send POST-REQUEST
        /// </summary>
        /// <param name="pJsonContent">Bytes of mp3, whitch should be recognized</param>
        /// <returns>HTTP response with recognized text</returns>
        private async Task<HttpResponseMessage> SendRequestToService(string pJsonContent)
        {
            HttpClient _Client = new HttpClient();
            var httpRequestMessage = new HttpRequestMessage();
            httpRequestMessage.Method = HttpMethod.Post;
            httpRequestMessage.RequestUri = new Uri(url);
            HttpContent httpContent = new StringContent(pJsonContent, Encoding.UTF8, "application/json");
            httpRequestMessage.Content = httpContent;

            return await _Client.SendAsync(httpRequestMessage);
        }

        /// <summary>
        /// Convert Mp3 To Wav (output File length will be x10 size longer - do not use memoryStream) 
        /// </summary>
        /// <param name="_inPath_">file FROM - MP3 </param>
        /// <param name="_outPath_">file TO - WAV </param>
        /// <param name="_rate">48000/24000/16000</param>
        /// <param name="_bit">16/32</param>
        /// <param name="_chanels">1/2</param>
        private static void ConvertMp3ToWav(string _inPath_, string _outPath_, int _rate, int _bit, int _chanels)
        {
            try
            {
                using (Mp3FileReader mp3 = new Mp3FileReader(_inPath_))
                using (var converter = WaveFormatConversionStream.CreatePcmStream(mp3))
                using (var upsampler = new WaveFormatConversionStream(new WaveFormat(_rate, _bit, _chanels), converter))
                {
                    WaveFileWriter.CreateWaveFile(_outPath_, upsampler);
                }
            }
            catch (Exception ex)
            {
                var wavBytes = File.ReadAllBytes(_inPath_);
                File.WriteAllBytes(_outPath_, wavBytes);
                /// TODO Global Log
                //Program.WhriteLogToFile("Error with ConvertMp3ToWav", ex);

            }
        }

        /// <summary>
        /// Convert Wav To Mp3
        /// </summary>
        /// <param name="_inPath_">*.wav file path</param>
        /// <param name="_outPath_">*.mp3 file path</param>
        private void ConvertWavToMP3(string waveFileName, string mp3FileName, int bitRate = 128)
        {
            Codec.WaveToMP3(waveFileName, mp3FileName, bitRate);
        }
        #endregion Staff
    }

    /// <summary>
    /// Convernet Wav to mp3 and mp3 to Wav, Coder-decoder
    /// </summary>
    public static class Codec
    {
        // Convert WAV to MP3 using libmp3lame library
        public static void WaveToMP3(string waveFileName, string mp3FileName, int bitRate = 128)
        {
            using (var reader = new AudioFileReader(waveFileName))
            using (var writer = new LameMP3FileWriter(mp3FileName, reader.WaveFormat, bitRate))
                reader.CopyTo(writer);
        }

        // Convert MP3 file to WAV using NAudio classes only
        public static void MP3ToWave(string mp3FileName, string waveFileName)
        {
            using (var reader = new Mp3FileReader(mp3FileName))
            using (var writer = new WaveFileWriter(waveFileName, reader.WaveFormat))
                reader.CopyTo(writer);
        }
    }

    /// <summary>
    /// Class for parsing voice string (wav byte array from FRONT END)
    /// </summary>
    public class VoiceString
    {
        public string strByteArray { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DialogProcesser
{
    /// <summary>
    /// What is role of speaker
    /// </summary>
    public enum SpeakerType
    {
        user,
        @operator
    }

    /// <summary>
    /// Who said
    /// </summary>
   public class SpeakerPerson
    {
        public TimeSpan Start;
        public TimeSpan End;
        public SpeakerType speakerType;
        public string Text;
    }
}

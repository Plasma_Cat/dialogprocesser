﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DialogProcesser
{
    /// <summary>
    /// Functions and other actions returns any message with infirmation about errors or successes
    /// </summary>
    public class MethodMessage
    {
        /// <summary>
        /// 1 - Correctly, 0 - Error
        /// </summary>
        public bool Success = true;

        /// <summary>
        /// CORRECT! - if realy correct, Any message - if error, or result of the method
        /// </summary>
        public object MessageData = "CORRECT!";

        /// <summary>
        /// Exception, if error
        /// </summary>
        public Exception ExceptionData = null;

        /// <summary>
        /// JSON serialized message object
        /// </summary>
        [JsonIgnore]
        public string JsonMessage
        {
            get { return Newtonsoft.Json.JsonConvert.SerializeObject(this); }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace DialogProcesser
{
    class Program
    {
        static void Main(string[] args)
        {
            string mypath = "C:\\Users\\AA\\Desktop\\TestCuted";
            Console.WriteLine($"Process mp3 files in '{mypath}'");
            
            var res = DialogWorkerSingleton.Instance.GetDialogJson(mypath).Result;

            Console.WriteLine(res);
            Console.ReadLine();
        }
    }
}
